#include "DHT.h"
#include <wiringPi.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>

using namespace com_att_mss_m2m_device;
void DHT::read()
{
  uint8_t laststate = HIGH;
  uint8_t counter = 0;
  uint8_t j = 0, i;
  float f; // fahrenheit
  char tmp[10];

  int bitstreams[5] = {0,0,0,0,0};
  printf("DHT driver reading from pin=%d\n",_signalPin);

  // pull pin down for 18 milliseconds
  pinMode(_signalPin, OUTPUT);
  digitalWrite(_signalPin, LOW);
  delay(18);
  // then pull it up for 40 microseconds
  digitalWrite(_signalPin, HIGH);
  delayMicroseconds(40);
  // prepare to read the pin
  pinMode(_signalPin, INPUT);

  // detect change and read data
  for ( i=0; i< MAXTIMINGS; i++)
  {
    counter = 0;
    while (digitalRead(_signalPin) == laststate)
    {
      counter++;
      delayMicroseconds(1);
      if (counter == 255)
      {
        break;
      }
    }
    laststate = digitalRead(_signalPin);

    if (counter == 255) break;

    // ignore first 3 transitions
    if ((i >= 4) && (i%2 == 0))
    {
      // shove each bit into the storage bytes
      bitstreams[j/8] <<= 1;
      if (counter > 16)
        bitstreams[j/8] |= 1;
      j++;
    }
  }

  // check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
  // print it out if data is good
  if ((j >= 40) &&
    (bitstreams[4] == ((bitstreams[0] + bitstreams[1] + bitstreams[2] + bitstreams[3]) & 0xFF)) )
  {
    f = bitstreams[2] * 9. / 5. + 32;
    printf("Humidity = %d.%d %% Temperature = %d.%d *C (%.1f *F)\n",
    bitstreams[0], bitstreams[1], bitstreams[2], bitstreams[3], f);
    memset(tmp, 0, sizeof(char)*sizeof(tmp));
    sprintf(tmp, "%d.%.2d", bitstreams[0], bitstreams[1]);
    this->_humidity=::atof(tmp);
    memset(tmp, 0, sizeof(char)*sizeof(tmp));
    sprintf(tmp, "%d.%.2d", bitstreams[2], bitstreams[3]);
    this->_temperature=::atof(tmp);
  }
  else
  {
    printf("Data not good, skip\n");
  }
}


float DHT::getHumidity()
{
  return _humidity;
}

float DHT::getTemperature()
{
  return _temperature;
}


DHT::DHT( int signalPin)
{
  if (wiringPiSetup () == -1)
  {
    printf("GPIO set up failed");
  }
  _humidity = -1;
  _temperature = -1;
  _signalPin = signalPin;
}

int main (void)
{
  printf ("DHT Test\n") ;
  DHT *dht = new DHT(1);

  while (1)
  {
    dht->read();
    delay(1000); // wait 1sec to refresh
  }
  delete dht;
  return 0 ;
}
