#ifndef _DHT_H
#define _DHT_H

#define MAXTIMINGS 85

namespace com_att_mss_m2m_device
{
  // #ifdef __cplusplus
  //         extern "C" {
  // #endif
  //         void sayHello ();
  // #ifdef __cplusplus
  //         }
  // #endif

  class DHT
  {
  public:
    void read();
    float getHumidity();
    float getTemperature();
    DHT(int signalPin);

  private:
    float _humidity;
    float _temperature;
    int _signalPin;
  };

}
#endif
