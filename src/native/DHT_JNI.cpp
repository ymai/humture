#include <jni.h>
#include <stdio.h>
#include "DHT.h"
#include <unistd.h>
#include "DHT11_JNI.h"

JNIEXPORT jfloatArray JNICALL Java_com_att_mss_m2m_device_DHT_nativeRead
	(JNIEnv * env, jobject jobj, jint pin)
{
	jfloatArray result;
	result = (env)->NewFloatArray(2);

	// out of memory
	if (result == NULL)
	{
		return NULL;
	}
	jfloat fill[2];

	using namespace com_att_mss_m2m_device;
	printf("JNI layer pin=%d\n", pin);
	DHT *dht = new DHT(pin);
	dht->read();
	fill[0] = dht->getHumidity();
	fill[1] = dht->getTemperature();

	// setting java object
	(env)->SetFloatArrayRegion(result, 0, 2, fill);
	delete dht;
	return result;
}
