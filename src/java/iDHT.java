package com.att.mss.m2m.device;

/**
* This is the interface for the DHT device driver.
*
* @author ymai
* @see    DHTFactory
* @see    DHT
* @see    DHTSimulator
* @since  1.0
*/
public interface iDHT
{
  public void read();
  public float getHumidity();
  public float getTemperature();
}
