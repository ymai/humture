package com.att.mss.m2m.device;
/**
* This is a standard Factory class that allows user to create an iDHT object
* It will check to see if the native library can be loaded. If load fail it will
* return a DHT simulator object.
*
* @author ymai
* @see    iDHT
* @see    DHT
* @see    DHTSimulator
* @since  1.0
*/
public class DHTFactory
{
  private static DHTFactory _factory = null;
  public static DHTFactory instantiate()
  {
    if (_factory != null)
      _factory = new DHTFactory();
    return _factory;
  }

  public static iDHT create(int pin)
  {
    try {
      DHT dht= new DHT(pin);
      return dht;
    } catch (UnsatisfiedLinkError e) {
      return new DHTSimulator();
    }
  }
}
