package com.att.mss.m2m.device;

/**
* This is a simulator object for the DHT class.  It is returned by the DHTFactory
* when loading the native library fail.
*
* @author ymai
* @see    iDHT
* @see    DHT
* @see    DHTFactory
* @since  1.0
*/
import org.slf4j.*;

public class DHTSimulator implements iDHT
{
  static Logger LOGGER = LoggerFactory.getLogger("DHT");
  public DHTSimulator()
  {
    LOGGER.info("DHT Java Simulator created");
  }

  public void read()
  {
  }

  public float getHumidity()
  {
    return 40;
  }

  public float getTemperature()
  {
    return 22;
  }
}
