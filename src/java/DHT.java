package com.att.mss.m2m.device;

/**
* This class is a wrapper class to the native DHT device driver, which reads
* the humidity and temperature through the DHT device.  User needs to specify
* input pin.
*
* @author ymai
* @see    iDHT
* @see    DHTFactory
* @see    DHTSimulator
* @since  1.0
*/
import org.slf4j.*;

public class DHT implements iDHT
{
  static Logger LOGGER = LoggerFactory.getLogger("DHT");
  private int _signalPin;
  private float _humidity, _temperature;
  DHT(int pin)
  {
    LOGGER.info("DHT Java layer driver created");
    _signalPin = pin;
  }

  public void read()
  {
    System.out.println("In read");
    float data[] = nativeRead(_signalPin);
    _humidity = data[0];
    _temperature = data[1];
  }

  public float getHumidity()
  {
    return _humidity;
  }

  public float getTemperature()
  {
    return _temperature;
  }

  native float[] nativeRead(int pin);
  static {
      System.loadLibrary("DHT"); /* (2) */
  }

  static public void main(String argv[]) {
    DHT dht = new DHT(1);
    float floatarray[];
    floatarray = dht.nativeRead(1);
    LOGGER.info(String.format("Float Array %f %f", floatarray[0],floatarray[1]));
    dht.read();
    LOGGER.info(String.format("Humidity=%f, temperature=%f", dht.getHumidity() ,dht.getTemperature()));
  }
}
