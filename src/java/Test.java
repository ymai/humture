package com.att.mss.m2m.device;

import org.slf4j.*;

public class Test
{
  static Logger LOGGER = LoggerFactory.getLogger("DHTTest");

  static public void main(String argv[])
  {
    iDHT dht = DHTFactory.create(1);
    if (dht == null)
    {
      LOGGER.error("DHTFactory create failed");
      System.exit(1);
    }
    dht.read();
    LOGGER.info("Humidity={} Temperature={}",dht.getHumidity(), dht.getTemperature());
  }
}
