Overview
==================
This package provides the driver for [DHT humidity sensor](https://www.sparkfun.com/products/10167)
and the library is designed to be used by RaspberryPi.  This is tested on RPI
B+ model.

The package creates the following files

* libDHT.so
* libDHT.jar
* DHT11_JNI.h

To-Do
======================
Wiring
-------
The following image illustrates the wiring requirement for the DHT.

![Wiring diagram missing, please reach out to me][WiringImage]

Assuming you are connecting to Pin 1 on the RPi, if you orient the RPi with the
GPIO on the top, pin 1 is top row 6th from the left.  Power 5V is the 1st from
the left, and ground is 3rd from the left.

Please feel free to reach out to [me](mailto:yusufmai@gmail.com) for help.

Getting Binaries
----------------
If not required to compile from the source, the binaries are available in the
download section.  Please download the followings :

* [Java archive](https://bitbucket.org/ymai/humture/downloads/libDHT-jar-with-dependencies.jar).
* [libDHT library](https://bitbucket.org/ymai/humture/downloads/libDHT.so)

On development machine
----------------------
1) mvn clean install
```bash
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building DHT_DeviceDriver 1.0.1-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.4.1:clean (default-clean) @ libDHT ---
[INFO] Deleting /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/target
[INFO]
[INFO] --- maven-resources-plugin:2.5:resources (default-resources) @ libDHT ---
[debug] execute contextualize
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/src/main/resources
[INFO]
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ libDHT ---
[INFO] Compiling 5 source files to /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/target/classes
[INFO]
[INFO] --- maven-resources-plugin:2.5:testResources (default-testResources) @ libDHT ---
[debug] execute contextualize
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/src/test/resources
[INFO]
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ libDHT ---
[INFO] No sources to compile
[INFO]
[INFO] --- maven-surefire-plugin:2.10:test (default-test) @ libDHT ---
[INFO] No tests to run.
[INFO] Surefire report directory: /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/target/surefire-reports

-------------------------------------------------------
T E S T S
-------------------------------------------------------

Results :

Tests run: 0, Failures: 0, Errors: 0, Skipped: 0

[INFO]
[INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ libDHT ---
[INFO] Building jar: /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/target/libDHT-1.0.1-SNAPSHOT.jar
[INFO]
[INFO] --- native-maven-plugin:1.0-alpha-8:javah (javah) @ libDHT ---
[INFO] /bin/sh -c cd /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture && javah -o /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/src/native/DHT_JNI.h -classpath /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/target/classes:/Users/yusufmai/.m2/repository/org/slf4j/slf4j-api/1.7.5/slf4j-api-1.7.5.jar:/Users/yusufmai/.m2/repository/org/slf4j/slf4j-simple/1.7.5/slf4j-simple-1.7.5.jar com.att.mss.m2m.device.DHT
[INFO]
[INFO] --- maven-assembly-plugin:2.5.3:single (make-assembly) @ libDHT ---
[INFO] Building jar: /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/libDHT-jar-with-dependencies.jar
[INFO]
[INFO] --- maven-install-plugin:2.3.1:install (default-install) @ libDHT ---
[INFO] Installing /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/target/libDHT-1.0.1-SNAPSHOT.jar to /Users/yusufmai/.m2/repository/com/att/mss/m2m/device/libDHT/1.0.1-SNAPSHOT/libDHT-1.0.1-SNAPSHOT.jar
[INFO] Installing /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/pom.xml to /Users/yusufmai/.m2/repository/com/att/mss/m2m/device/libDHT/1.0.1-SNAPSHOT/libDHT-1.0.1-SNAPSHOT.pom
[INFO] Installing /Users/yusufmai/Documents/0users/AppDev/RaspBerryPi/Humture/libDHT-jar-with-dependencies.jar to /Users/yusufmai/.m2/repository/com/att/mss/m2m/device/libDHT/1.0.1-SNAPSHOT/libDHT-1.0.1-SNAPSHOT-jar-with-dependencies.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 4.708s
[INFO] Finished at: Thu Feb 26 00:29:05 EST 2015
[INFO] Final Memory: 14M/34M
[INFO] ------------------------------------------------------------------------
```

2) scp src/native/*.cpp src/native/*.h <RPI_USER>@<IP_ADDR_OF_RPI>:<dest_dir>/src/native
This assumes the folder src/native has been created in a destination directory
of your choice.
```bash
pi@172.16.0.103's password:
DHT.cpp                                                                                                                   100% 2341     2.3KB/s   00:00
DHT.h                                                                                                                     100%  446     0.4KB/s   00:00
DHT11_JNI.cpp                                                                                                             100%  627     0.6KB/s   00:00
DHT11_JNI.h                                                                                                               100%  485     0.5KB/s   00:00
```
3) scp Makefile libDHT-jar-with-dependencies.jar <RPI_USER>@<IP_ADDR_OF_RPI>:<dest_dir>
```bash
pi@172.16.0.103's password:
Makefile                                                                                                                  100% 2055     2.0KB/s   00:00
libDHT-jar-with-dependencies.jar                                                                                          100% 1417     1.4KB/s   00:00
```



On RaspberryPi
-------------
1) make clean
```bash
[Clean] DHT.o DHT11_JNI.o libDHT.so
```
2) sudo make uninstall
```bash
[Uninstalling Headers]
[Uninstalling library]
[Uninstalling Jar]
```
Note: you may see some error message that you can ignore

3) make
```bash
[Compile CPP] DHT.cpp
[Compile CPP] DHT11_JNI.cpp
[Link]
[Compile Test]
```
4) sudo make install
```bash
[Installing Headers]
[Installing library]
[Installing Jar]
```

[WiringImage]: https://bitbucket.org/ymai/humture/raw/master/DHT-wiring.gif
"Wiring schematic for DHT"
