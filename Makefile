DESTDIR=/usr
PREFIX=/local
JARSHARE=/share/java

#DEBUG	= -g -O0
DEBUG	= -O2
CC	= g++
INCLUDE	= -I$(DESTDIR)$(PREFIX)/include  -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/linux
CFLAGS	= $(DEBUG) -Wall $(INCLUDE) -std=c++0x
LDFLAGS	= -L$(DESTDIR)$(PREFIX)/lib
NATIVESRC_DIR = src/native
JAVASRC_DIR = src/java
JAVATARGET_DIR =  target/classes
#java target dir to match maven
SRC	= $(NATIVESRC_DIR)/DHT.cpp $(NATIVESRC_DIR)/DHT_JNI.cpp
OBJ	= DHT.o DHT_JNI.o
#OBJ = $(SRC:.cpp=.o) $(SRC:.c=.o)
LIBS	= -lwiringPi
#JAVAPACKAGE = com/att/mss/m2m/device
#JAVACLASS = $(JAVAPACKAGE)/DHT.class DHT_JNI.

all:		libDHT.so DHTTest
#java:		libDHT.jar

libDHT.so:	$(OBJ)
		@echo [Link]
		@$(CC) $(DEBUG) -o $@  $(OBJ) $(LDFLAGS) -shared $(LIBS)

DHTTest:	$(OBJ)
		@echo [Compile Test]
		@$(CC) $(DEBUG) -o $@  $(OBJ) $(LDFLAGS) $(LIBS)

#libDHT.jar:	$(JAVAPACKAGE)/DHT.class
#		@echo [Archiving Java class]
#		@jar -cvf libDHT.jar -C $(JAVATARGET_DIR) com

# Compiling java class in make file because it may be required to compile on a
# controller that is not powerful enough to support mvn.
$(JAVAPACKAGE)/%.class:	$(JAVASRC_DIR)/%.java
		@echo [Compile java]
		@mkdir -p $(JAVATARGET_DIR)
		@javac -d $(JAVATARGET_DIR) $(JAVASRC_DIR)/*.java


$(NATIVESRC_DIR)/DHT_JNI.h: libDHT.jar
		@echo [Generating JNI header]
		@javah -o $@ -classpath libDHT.jar:. com.att.mss.m2m.device.DHT

%.o: $(NATIVESRC_DIR)/%.cpp
	@echo [Compile CPP] $<
	@$(CC) -c $(CFLAGS) $< -o $@

.PHONY:	install
install:
	@echo "[Installing Headers]"
	@install -m 0755 -d $(DESTDIR)$(PREFIX)/include
	@install -m 0644 $(NATIVESRC_DIR)/DHT_JNI.h $(DESTDIR)$(PREFIX)/include/DHT_JNI.h
	@install -m 0644 $(NATIVESRC_DIR)/DHT.h $(DESTDIR)$(PREFIX)/include/DHT.h

	@echo "[Installing library]"
	@install -m 0755 -d $(DESTDIR)$(PREFIX)/lib
	@install -m 0755 libDHT.so $(DESTDIR)$(PREFIX)/lib/libDHT.so

# This is done is a external jar fashion where jar is linked into the business
# logic code.  Maven pom file is also available so business logic code and
# device JNI jar can all be bundled into one jar file.
	@echo "[Installing Jar]"
	@install -m 0755 -d $(DESTDIR)$(JARSHARE)
	@install -m 0755 libDHT-jar-with-dependencies.jar $(DESTDIR)$(JARSHARE)/libDHT.jar
#	@mvn org.apache.maven.plugins:mave-install-plugin:2.3.1:install-file --Dfile=libDHT.jar  -DgroudId=com.att.mss.m2m.device -DartifactId=DHT -Dversion=1.0.0-SNAPSHOT -Dpackageing=jar

.PHONY:	uninstall
uninstall:
	@echo "[Uninstalling Headers]"
	-@rm $(DESTDIR)$(PREFIX)/include/DHT_JNI.h
	-@rm $(DESTDIR)$(PREFIX)/include/DHT.h

	@echo "[Uninstalling library]"
	-@rm $(DESTDIR)$(PREFIX)/lib/libDHT.so

	@echo "[Uninstalling Jar]"
	-@rm $(DESTDIR)$(JARSHARE)/libDHT.jar


.PHONY:	clean
clean:
	@echo "[Clean] $(OBJ) libDHT.so"
	@rm -f $(OBJ) libDHT.so DHTTest *~ core tags

.PHONY:	cleanJava
cleanJava:
	@echo "[Clean]  $(JAVACLASS) libDHT.jar"
	@rm -f libDHT.jar
	@rm -rf $(JAVATARGET_DIR)
